package co.simplon.promo16.spotiplon.entity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Artist {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String style;
    private String picture;
    private LocalDate started;
    @OneToMany(mappedBy = "artist")
    private List<Album> albums = new ArrayList<>();

    
    public Artist(String name, String style, String picture, LocalDate started) {
        
        this.name = name;
        this.style = style;
        this.picture = picture;
        this.started = started;
    }
    public Artist(Integer id, String name, String style, String picture, LocalDate started) {
        this.id = id;
        this.name = name;
        this.style = style;
        this.picture = picture;
        this.started = started;
    }
    public Artist() {
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getStyle() {
        return style;
    }
    public void setStyle(String style) {
        this.style = style;
    }
    public String getPicture() {
        return picture;
    }
    public void setPicture(String picture) {
        this.picture = picture;
    }
    public LocalDate getStarted() {
        return started;
    }
    public void setStarted(LocalDate started) {
        this.started = started;
    }
    public List<Album> getAlbums() {
        return albums;
    }
    public void setAlbums(List<Album> albums) {
        this.albums = albums;
    }
}
